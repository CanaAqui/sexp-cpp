[![Build Status](https://travis-ci.org/lispparser/sexp-cpp.svg?branch=master)](https://travis-ci.org/lispparser/sexp-cpp)

SExp - A S-Expression Parser for C++
====================================

SExp is an [S-Expression](https://en.wikipedia.org/wiki/S-expression) parser for C++.

Example Code:

    sexp::Value value = sexp::Parser::from_string("(1 2 3 4 5)")
    sexp::Value const& head = value.get_car();
    sexp::Value const& tail = value.get_cdr();
    if (head.is_integer())
    {
       std::cout << head.as_int() << std::endl;
    }

Arrays vs Cons
--------------

When dealing with large amounts of data the classic Scheme list comes
with quite abit of a performance penentaly, for this reason SExp
allows you to interpret all list as arrays:

    sexp::Parser::from_stream(fin, sexp::Parser::USE_ARRAYS);


C++ locales
-----------

If C++ locales are used in your code, compile with:

    cmake -DSEXP_USE_LOCALE=ON

Otherwise the input and output functions will produce incorrect
results (i.e. "1,5" instead of "1.5").

Util
----

Library has number of useful utilities, some of them has caveats.

### Car/cdr related

Instead of calling `val.get_car()`/`val.get_cdr()`, it is possible to use
`sexp::car(val)`/`sexp::cdr(val)`. Even things like cddaar supported but in
reverse order, e.g. cadr in lisp returns car of a cdr of a value (e.g. inner
letters are backward, first we get cdr and than we get car) where in the library
those inner letters are in unusial for lispers way.

### List utilities

`sexp::list_length(value)` will try to return list length but can be slow for
long list because it traverses to the end. Even more, it's done recursively, so
the stack will grow as well.

`list_ref` will return list element with given index. Speed and stack works the
same as for `list_length`.

### ListIterator

`sexp::ListAdapter` makes STL iterations very easy. Given that `value` is list,
it's possible to iterate following way:

    for (const sexp::Value& cell: sexp::ListAdapter(mySexpList)) {
        process(cell);
    }
